<?php

namespace BonchDev\uCaller;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;

/**
 * uCaller class
 *
 * @property-read boolean $status Call status
 * @property-read integer $ucaller_id uCaller ID of call
 * @property-read integer $phone The phone that was called
 * @property-read integer $code The code in the end of phone number
 * @property-read string $client Client Nickname
 * @property-read string $unique_request_id Idempotence ID (if was sended)
 * @property-read boolean $exists Is call exists (when using idempotence key)
 * @property-read boolean $free_repeated Is call was repeated for free
 *
 * @property-read integer $init_time
 * @property-read integer $call_status
 * @property-read boolean $is_repeated
 * @property-read boolean $repeatable
 * @property-read integer $repeat_times
 * @property-read array $repeated_ucaller_ids
 * @property-read string $country_code
 * @property-read string $country_image
 * @property-read object $phone_info
 * @property-read float $cost
 *
 * @property-read float $rub_balance
 * @property-read float $bonus_balance
 * @property-read string $tariff
 * @property-read string $tariff_name
 */
class uCaller
{
    /** @var string */
    private $serviceID;
    /** @var string */
    private $key;

    /** @var array */
    public $query;

    /** @var Response */
    public $response;

    public function __construct(
        string $serviceID,
        string $key
    ) {
        $this->serviceID = $serviceID;
        $this->key = $key;
    }

    /**
     * HTTP client
     *
     * @return Client
     */
    private function http(): Client
    {
        return new Client([
            'base_uri' => 'https://api.ucaller.ru/v1.0/',
        ]);
    }

    /**
     * Query params
     *
     * @param array $args
     * @return array
     */
    private function query(array $args = []): array
    {
        $query = [];

        $query['service_id'] = $this->serviceID;
        $query['key'] = $this->key;

        if ($args != []) {
            foreach ($args as $key => $value) {
                if ($value) {
                    $query[$key] = $value;
                }
            }
        }

        $this->query = $query;

        return $query;
    }

    /**
     * InitCall to phone with code
     *
     * @param string $phone Phone that will be called
     * @param string $code Code that will be in the end
     * @param string $client Client nickname
     * @param string $unique Unique ID for idempotence
     * @return self
     */
    public function initCall(
        $phone,
        $code = null,
        string $client = null,
        string $unique = null
    ): self {
        $query = $this->query([
            'phone' => $phone,
            'code' => $code,
            'client' => $client,
            'unique' => $unique,
        ]);

        $response = ($this->http())
            ->get('initCall', [
                'query' => $query,
            ]);

        $this->response = $response;

        return $this;
    }

    /**
     * InitRepeat call
     *
     * @param integer $uCallerID ucaller_id of exists call
     * @return self
     */
    public function initRepeat(int $uCallerID): self
    {
        $query = $this->query([
            'uid' => $uCallerID,
        ]);

        $response = ($this->http())
            ->get('initRepeat', [
                'query' => $query,
            ]);

        $this->response = $response;

        return $this;
    }

    /**
     * Get info about call
     *
     * @param integer $uCallerID ucaller_id of exists call
     * @return self
     */
    public function getInfo(int $uCallerID): self
    {
        $query = $this->query([
            'uid' => $uCallerID,
        ]);

        $response = ($this->http())
            ->get('getInfo', [
                'query' => $query,
            ]);

        $this->response = $response;

        return $this;
    }

    /**
     * Get balance of account
     *
     * @return self
     */
    public function getBalance(): self
    {
        $query = $this->query();

        $response = ($this->http())
            ->get('getBalance', [
                'query' => $query,
            ]);

        $this->response = $response;

        return $this;
    }

    public function __get($name)
    {
        return $this->responseBody()->$name ?? null;
    }

    /**
     * Return response body as object
     *
     * @return object
     */
    public function responseBody(): object
    {
        return json_decode($this->response->getBody());
    }
}
