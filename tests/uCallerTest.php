<?php

namespace BonchDev\uCaller\Tests;

use BonchDev\uCaller\uCaller;
use PHPUnit\Framework\TestCase;

class uCallerTest extends TestCase
{
    /** @var uCaller */
    private $uCaller;

    protected function setUp(): void
    {
        parent::setUp();

        $this->uCaller = new uCaller(
            '', // your service_id here
            '' // your key here
        );
    }

    public function testInitCall()
    {
        $response = $this->uCaller->initCall(
            '', // your phone here
            '1234'
        );

        $this->assertTrue(
            $response->responseBody()->status
        );
    }

    public function testInitRepeat()
    {
        $response = $this->uCaller->initRepeat(
            1187876
        );

        var_dump($response->responseBody());

        $this->assertTrue(
            $response->responseBody()->status
        );
    }

    public function testGetInfo()
    {
        $response = $this->uCaller->getInfo(
            1187876
        );

        var_dump($response->responseBody());

        $this->assertTrue(
            $response->responseBody()->status
        );
    }

    public function testGetBalance()
    {
        $response = $this->uCaller->getBalance();

        var_dump($response->responseBody());

        $this->assertTrue(
            $response->responseBody()->status
        );
    }

    public function testGetter()
    {
        $response = $this->uCaller->getBalance();

        var_dump($response->responseBody());

        $this->assertTrue(
            $response->status
        );

        $this->assertNull(
            $response->test
        );
    }
}
